package com.upload.entity;

import java.io.Serializable;

public class FileInfo implements Serializable {

	/**
	 * 文件名
	 */
	private String name;

	/**
	 * 文件的上傳時間
	 */
	private String uploadTimeStr;

	/**
	 * 上传时间
	 */
	private long uploadTime;

	/**
	 * 文件全名
	 */
	private String fullName;

	/**
	 * 文件大小 (显示大小)
	 */
	private String fileLengthStr;

	private double fileLength;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUploadTimeStr() {
		return uploadTimeStr;
	}

	public void setUploadTimeStr(String uploadTimeStr) {
		this.uploadTimeStr = uploadTimeStr;
	}

	public long getUploadTime() {
		return uploadTime;
	}

	public void setUploadTime(long uploadTime) {
		this.uploadTime = uploadTime;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getFileLengthStr() {
		return fileLengthStr;
	}

	public void setFileLengthStr(String fileLengthStr) {
		this.fileLengthStr = fileLengthStr;
	}

	public double getFileLength() {
		return fileLength;
	}

	public void setFileLength(double fileLength) {
		this.fileLength = fileLength;
	}

}
