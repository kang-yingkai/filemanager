<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8" isELIgnored="false"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
<head>
<title>消息提醒</title>
</head>

<body>
	<%@include file="top.jsp"%>
	<hr>
	<h3>${message}</h3>
	<br>

	<c:if test="${downloadUrl !=null }">
		<a href="${ downloadUrl}">下载此文件</a>
	</c:if>

</body>
</html>