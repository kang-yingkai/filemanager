<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8" isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML>

<html>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
<head>
<title>下载文件显示页面</title>
</head>

<style>
</style>

<body>
	<%@include file="top.jsp"%>
	<hr>
	<h2>文件列表</h2>
	<table border=1 style="border-collapse: collapse;">
		<tr style="background-color: #9e9e9e9c;">
			<td width="200">文件名</td>
			<td width="160">文件大小</td>
			<td width="160">上传时间</td>
			<td width="100">操作</td>
		</tr>
		<!-- 遍历Map集合 -->
		<c:forEach var="f" items="${listFile}">
			<tr>
				<td><a href="DownLoadServlet?filename=${f.fullName}"
					style="text-decoration: none">${f.name}</a></td>
				<td>${f.fileLengthStr }</td>
				<td>${f.uploadTimeStr}</td>
				<td><a href="DownLoadServlet?filename=${f.fullName}"><button >下载</button></a>&nbsp;&nbsp;&nbsp;<a
					href="DeleteServlet?filename=${f.fullName}"><button >删除</button></a></td>
			</tr>

		</c:forEach>
	</table>
	<hr>
	上传文件总数:${fileCount},文件总大小:${totalLength} MB
	<br>
	<font color="red">注:点击文件名可下载</font>
</body>
</html>