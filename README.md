# 文件管理

#### 项目介绍
文件管理
访问地址:[http://localhost:8000/filemanager/](http://localhost:8000/filemanager/)

#### 软件架构
maven项目


#### 启动说明
一，在IDEA中启动
pom文件里已配置tomcat启动插件,在idea可以点击maven>tomcat:run

二、打包运行
直接达成war包，放在tomcat下面的webapps下面运行

#### 说明
保存的文件位置在WEB-INF/upload下面，根据需要可以更改路径

#### 效果截图

![](https://gitee.com/lengcz/filemanager/raw/master/docs/1.png)
![](https://gitee.com/lengcz/filemanager/raw/master/docs/2.png)
![](https://gitee.com/lengcz/filemanager/raw/master/docs/3.png)
![](https://gitee.com/lengcz/filemanager/raw/master/docs/4.png)
#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)